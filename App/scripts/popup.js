const loadButton = document.querySelector("#load");
const lists = document.querySelector("#lists-container");
const headerItemsContainer = document.querySelector("#header-items-container");
const ListKey       = "twitter-list";
const headerKey     = "twitter-header";
const URLKey        = "current_url";
var constructedItems = " constructed";
var listItemClass   = "list_item";
var headerItemClass = "header_item";


/**
 *  functions about the list of twitter lists
 */
var ListManagement = {
    /**
     * send a message to the content script parse the web page searching for the list
     * get the list, store it locally and display it
     */
    loadFromPage() {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {action:"lists"}, function(response){
                ListManagement.displayList(response);
                ListManagement.storeToLocal(response);
            });
        });
    },

    loadFromStorage() {
        ListManagement.displayList( ListManagement.getFromLocalStorage() );
    },

    /**
     * create a list item (html link) with the provided parameters
     * 
     * @param {String} name 
     * @param {String} link 
     */
    displayItem(name, link) {
        let elem = document.createElement("a");
        elem.href = link;
        elem.innerText = name;
        elem.className = listItemClass + constructedItems;

        let container = document.querySelector("#lists-container");
        lists.appendChild(elem);
    },

    /**
     * display the lists in the popup by iterating through 
     * an array of objects that contains them.
     * this uses displayItem() to construct the HTML element and append it into the popup.
     * 
     * @param {Array(Object)} lst array of objects with a key and value 
     */
    displayList(lst) {
        if (!(Array.isArray(lst)) || lst === 'undefined' || lst.length === 0 || lst === null) return;
        for (let i = 0 ; i < lst.length ; i++) {
            let item = lst[i];
            ListManagement.displayItem(item.text, item.href);
        }
    },


    /**
     * stringify the list of items then store it in the local storage
     */
    storeToLocal(l) {
        let lst_str = JSON.stringify(l);
        localStorage.setItem(ListKey, lst_str);
    },

    /**
     * just get the list of items from the local storage, parse it and return it
     */
    getFromLocalStorage() {
        let l = localStorage.getItem(ListKey);
        return JSON.parse( l );
    },

    /**
     * delete the list of lists from the popup
     */
    deleteFromPopup() {
        let lst = document.querySelectorAll(`.${listItemClass}`);
        if (lst.length === 0) return;
        for (let i = 0 ; i < lst.length ; i++) {
            lst[i].remove();
        }
    },

    deleteFromLocalStorage() {
        localStorage.removeItem(ListKey);
    },

    isLocallyStored() {
        return isLocallyStored(ListKey);
    }
};


/**
 *  functions about the 3 buttons of the header
 */
var HeaderManagement = {
    /**
     * elements of the header: Home  |  profile  |  lists
     * construct each element (with constructSingleElement())
     * put them inside and array and send them to displayElements() to be displayed
     */
    loadElementsFromPage() {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {action:"username"}, function(response){
                if (Array.isArray(response) && response.length === 0) return;
                let ar = HeaderManagement.constructAllElements(response);
                HeaderManagement.displayElements( ar );
                HeaderManagement.saveElementsToLocalstorage( response );    // save the username
            });
        });
    },

    /**
     * 
     * @param {String} name 
     * @returns {Array}
     */
    constructAllElements(name) {
        let ar = new Array();
        ar.push(HeaderManagement.constructSingleElement("Home", "https://www.twitter.com/"));
        ar.push(HeaderManagement.constructSingleElement("Profile", `https://www.twitter.com/${name}/`));
        ar.push(HeaderManagement.constructSingleElement("Lists", `https://www.twitter.com/${name}/lists`));
        return ar;
    },

    constructSingleElement(name, link) {
        let elem = document.createElement("a");
        elem.innerText = name;
        elem.href = link;
        elem.className = headerItemClass;
        return elem;
    },

    /**
     * add the received element into the popup page to display them
     * @param {Array} lst     array of 3 html elements (links)
     */
    displayElements(lst) {
        let lngth = lst.length;
        if (lngth === 0) return;
        for (let i = 0 ; i < lngth ; i++) 
            headerItemsContainer.appendChild(lst[i]);
    },

    loadElementsFromLocalstorage() {
        let elems = HeaderManagement.constructAllElements( localStorage.getItem(headerKey) );        
        HeaderManagement.displayElements( elems );
    },

    /**
     * 
     * @param {String} elem
     */
    saveElementsToLocalstorage(name) {
        localStorage.setItem(headerKey, name);
    },

    /**
     * delete the header items inside the popup
     */
    deleteFromPupup() {
        let lst = document.querySelectorAll(`.${headerItemClass}`);
        if (lst.length === 0) return;
        for (let i = 0 ; i < lst.length ; i++) {
            lst[i].remove();
        }
    },

    deleteFromLocalStorage() {
        localStorage.removeItem(headerKey);
    },

    isLocallyStored() {
        return isLocallyStored(headerKey);
    }
};

/**
 *  collection of functions that displays errors on the console 
 *  and the popup page, and clears the popup page from errors
 */
var Errors = {

    /**
     * display an error message in the console by sending it to the content script
     * @param {String} err 
     */
    toConsole(err) {
        sendMessage("print", err);
        // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        //     chrome.tabs.sendMessage(tabs[0].id, {action:"print", msg: err}, function(response){
        //     });
        // });
    },

    /**
     * append an error message into the popup page
     * @param {*} s 
     */
    append(s) {
        let li = document.createElement("li");
        li.innerText = s;
        li.style.color = "red";
        let cont = document.querySelector("#error-list");
        cont.appendChild(li);
    },

    /**
     * remove all the errors from the popup page
     */
    clear() {
        let nodes = document.querySelector("#error-list").childNodes;
        nodes.forEach(function(i) { i.remove(); });
    }
};

/**
 *  functions to ask, store, get and remove the tab url
 */
var URL = {
    store(url) {
        sessionStorage.setItem(URLKey, url);
    },

    get() {
        return sessionStorage.getItem(URLKey);
    },

    clear() {
        sessionStorage.removeItem(URLKey);
    },

    /**
     *  verify if the current URL is the twitter list page
     */
    inListPage() {
        URL.getCurrentURL();
        let url = URL.get();
        let reg = /^(http(s)?:\/\/)?((w){3}.)?twitter(\.com)?\/.+\/lists/;
        return reg.test(url);
    },

    /**
     * send a message to content.js and receive the URL of the page
     * @return {String}
     */
    getCurrentURL() {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {action:"current url"}, function(response){
                URL.store(response);
            });
        });
    }
};


/**
 * called when the popup button is clicked
 * set the popup UI according to the current tab.
 */
document.addEventListener('DOMContentLoaded', function() {
    URL.getCurrentURL();

    if (HeaderManagement.isLocallyStored() && ListManagement.isLocallyStored()) {
        HeaderManagement.loadElementsFromLocalstorage();
        ListManagement.loadFromStorage();
        setTimeout(attachLinks, 50);    // add the possibility of all links to be only clicked to open in a new tab
    } else {
        let msg = "It seams that you haven't stored any data. \
        please go to the list page in your twitter account \
        then come back to the extension and click on the Load button.";
        Errors.append(msg);
    }
});

/**
 * execute when the button Load is clicked
 */
loadButton.addEventListener("click", function() {
    let lst = URL.inListPage();
    if (lst) {
        Errors.clear();
        HeaderManagement.deleteFromPupup();
        ListManagement.deleteFromPopup();
        HeaderManagement.loadElementsFromPage();
        ListManagement.loadFromPage();
        setTimeout(attachLinks, 50);    //delay attachLinks()
    } else {
        let msg = "ERROR. go to the twitter's list page (in your profile) then click Load";
        Errors.append(msg);
    }
});


/**
 * verify if a ket is stored in the local storage
 * @param {String} key 
 */
function isLocallyStored(k) {
    return !(localStorage.getItem(k) === null);
}
/**
 *  abstraction function to clear all the stored items
 * NOTE: this is used mainly for test purposes
 */
function clearStoredItems() {
    HeaderManagement.deleteFromLocalStorage();
    ListManagement.deleteFromLocalStorage();
    URL.clear();
}

/**
 * fetch all the links of the popup page 
 * and put on them an event that is triggered once they are clicked.
 * the message will be sent to the content script with the link to be opened
 */
function attachLinks() {
    let a = document.getElementsByTagName("a");
    for (let i = 0 ; i < a.length ; i++) {
        a[i].onclick = function() {
            sendMessage("open link", a[i].toString());
        }
    }
}
/**
 *  send a message to the content script
 * @param {String} act      the action to make
 * @param {Object} cont     the type of the object to be sent doesn't matter
 */
function sendMessage(act, cont) {
    let obj = { action:act, msg: cont };
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, obj, function(){
        });
    });
}