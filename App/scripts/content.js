
chrome.runtime.onMessage.addListener(function(response, sender, sendResponse) {
    
    if (response.action == "lists") {
        sendResponse(getList());
    }
    else if (response.action == "names") {
        let names = getNames();
        sendResponse( names );
    }
    else if (response.action == "username") {
        let usr = getUsername().substr(1);      // remove the '@'
        sendResponse( usr );
    }
    else if (response.action == "fullname") {
        sendResponse( getFullname() );
    }
    else if (response.action == "current url") {
        sendResponse(document.baseURI);
    }
    else if (response.action == "print") {
        console.log(response.msg),
        sendResponse("");
    }
    else if (response.action == "open link") {
        window.open(response.msg);
    }
    else {
        let msg = "content.js: something went wrong";
        console.log(msg);
        sendResponse(msg);
        alert(msg);
    }
});


/**
 * query the list of the html elements (links) that contains the twitter lists
 * iterate through them and extract the name of each list and its link
 * put each one into an object (by calling makeObject())
 * @returns {Array(Object)}     return an array of objects. each one of them represents a twitter list.
 */
function getList() {
    var pure = document.querySelectorAll(".ProfileListItem-name");
    var lst = new Array();
    for (let i = 0 ; i < pure.length ; i++) {
        let item = pure[i];
        lst.push( makeObject(item.innerText, item.href) );
    }
    return lst.sort(compare);
}

/**
 *  Construct an object from the name of the list and its URL
 * 
 * @param {String} content the name of the list
 * @param {String} link    the URL of the list
 * @returns {Object}       object with 2 keys  
 */
function makeObject(content, link) {
    let obj = {
        text: content,
        href: link
    };
    return obj;
}

/** 
 * test function that displays the list in the console
*/
function displayList() {
    var lst = getList();
    for (let i = 0 ; i < lst.length ; i++) {
        let obj = lst[i];
        console.log(`${obj.text} : ${obj.href}`);
    }
}

function getUsername() {
    let usr = document.querySelectorAll(".username");
    return usr[1].innerText;
}

function getFullname() {
    let usr = document.querySelectorAll(".fullname");
    return usr[1].innerText;
}

function getNames() {
    let usr = getUsername();
    let full = getFullname();
    let names = {
        username: usr,
        fullname: full
    };
    return names;
}

/**
 * a function that will be passed into Array.sort() to sort the lists alphabetically
 */
function compare(a, b) {
    const txtA = a.text.toUpperCase();
    const txtB = b.text.toUpperCase();
    
    let comparison = 0;
    if (txtA > txtB)
      comparison = 1;
    else if (txtA < txtB)
      comparison = -1;
    
    return comparison;
}
